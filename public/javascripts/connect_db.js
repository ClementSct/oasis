/* This JavaScript code snippet is establishing the connection to the PostgreSQL database using the `pg`
library. */
const { Client } = require('pg')
const client = new Client({
  user: 'postgres',
  host: 'postgres',
  database: 'OASIS_26_02_2024',
  password: 'postgres',
  port: 5432,
})

client.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});