/**
 * The JavaScript code defines a map application with multiple layers including satellite imagery and
 * vector layers representing different years, with functionality to toggle between different basemaps
 * and adjust layer opacity.
 * @param event - The `event` parameter in JavaScript represents the event that occurred, such as a
 * click, change, input, etc. It contains information about the event like the target element, type of
 * event, and any additional data related to the event. In event handling functions, you can access
 * this parameter to perform
 */
// Imports
let {Map, View} = ol;
let TileLayer = ol.layer.Tile;
let XYZ = ol.source.XYZ;
let VectorSource = ol.source.Vector;
let {transform} = ol.proj;
let GeoJSON = ol.format.GeoJSON;
let VectorLayer = ol.layer.Vector;
let Style = ol.style.Style;
let Fill = ol.style.Fill;
let Stroke = ol.style.Stroke;
let {getUid} = ol.util;


// Définition du centre de la carte
const center = transform([-1.0, 44.5], 'EPSG:4326', 'EPSG:3857');
const basic_view = new View({
  center: center,
  zoom: 9,
  minZoom : 6,
  maxZoom : 14
})


// Couche image satellite
var image_basemap = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
      'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
    url:
      'https://server.arcgisonline.com/ArcGIS/rest/services/' +
      'World_Imagery/MapServer/tile/{z}/{y}/{x}',
  }),
});

// Couche carte 1
var map_basemap_1 = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
      'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
    url:
      'https://server.arcgisonline.com/ArcGIS/rest/services/' +
      'World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
  }),
});

// Couche carte 2
var map_basemap = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012',
    url:
    'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
  }),
});


// Instanciation de la carte
const map = new Map({
  target: 'map',
  layers: [
    image_basemap,// image satellite comme fond de carte par défaut
  ],
  view: basic_view
});

image_basemap.setZIndex(0);


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Carte

// Création des interactions avec les boutons de fond de carte 
const choix_img_sat = document.getElementById('choix_img_sat');
choix_img_sat.addEventListener("change", verifCocheImg);

const choix_carte = document.getElementById('choix_carte');
choix_carte.addEventListener("change", verifCocheCarte);


/**
 * Ajout et suppresion du fond de carte OSM
 * @param {object} event - Évènement
 */
function verifCocheCarte(event){

  event.preventDefault();
  if(choix_carte.checked){
    map.addLayer(map_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(image_basemap);
    choix_img_sat.checked = false;
  }
  if(!choix_carte.checked){
    map.addLayer(image_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(map_basemap);
    choix_img_sat.checked = true;
  }
}

/**
 * Ajout et suppresion du fond de carte satellite
 * @param {object} event - Évènement
 */
function verifCocheImg(event) {
    event.preventDefault();
  
    // Affichage ou masquage du fond de carte
    if (choix_img_sat.checked) {
      map.addLayer(image_basemap);
      map_basemap.setZIndex(0);
      map.removeLayer(map_basemap);
      choix_carte.checked = false;
    } else {
      map.addLayer(map_basemap);
      map_basemap.setZIndex(0);
      map.removeLayer(image_basemap);
      choix_carte.checked = true;
    }
  
    // Affichage ou masquage des couches supplémentaires
    const couches = [couche_1990, couche_2000, couche_2006, couche_2012];
    const checked = choix_img_sat.checked;
  
    couches.forEach((couche) => {
      couche.setVisible(!checked);
    });
  }
  

document.addEventListener('DOMContentLoaded', (event) => {
    const bouton_1990 = document.getElementById('bouton_1990');
    const bouton_2000 = document.getElementById('bouton_2000');
    const bouton_2006 = document.getElementById('bouton_2006');
    const bouton_2012 = document.getElementById('bouton_2012');

    const couche_1990 = new VectorLayer({
        source: new VectorSource({
        format: new GeoJSON(), 
        url: 'couches_remonter_temps/90_final.geojson',
        }),
        style: style_couche_1990,
        opacity: 1, // Opacité initiale
    });
    
    const couche_2000 = new VectorLayer({
        source: new VectorSource({
        format: new GeoJSON(), 
        url: 'couches_remonter_temps/00_final.geojson', 
        }),
        style: style_couche_2000,
        opacity: 1, // Opacité initiale
    });
    
    const couche_2006 = new VectorLayer({
        source: new VectorSource({
        format: new GeoJSON(), 
        url: 'couches_remonter_temps/06_final.geojson', 
        }),
        style: style_couche_2006,
        opacity: 1, // Opacité initiale
    });
    
    const couche_2012 = new VectorLayer({
        source: new VectorSource({
        format: new GeoJSON(), 
        url: 'couches_remonter_temps/12_final.geojson', 
        }),
        style: style_couche_2012,
        opacity: 1, // Opacité initiale
    });

    

    bouton_1990.addEventListener('change', () => toggleCouche(bouton_1990, couche_1990));
    bouton_2000.addEventListener('change', () => toggleCouche(bouton_2000, couche_2000));
    bouton_2006.addEventListener('change', () => toggleCouche(bouton_2006, couche_2006));
    bouton_2012.addEventListener('change', () => toggleCouche(bouton_2012, couche_2012));


    // Ajoute la couche directement au chargement de la page
    toggleCouche(bouton_1990, couche_1990);

  
    // Fonction générique pour activer/désactiver une couche en fonction de l'état d'un bouton
    function toggleCouche(bouton, couche) {
        if (bouton.checked) {
        map.addLayer(couche);
        couche.setZIndex(2); // ajout de la couche par-dessus le fond de carte 
        } else {
        map.removeLayer(couche);
        }
    }

    // Ajout de la transparence 
    const opaciteBar1990 = document.getElementById('opaciteBar1990');
    const opaciteBar2000 = document.getElementById('opaciteBar2000');
    const opaciteBar2006 = document.getElementById('opaciteBar2006');
    const opaciteBar2012 = document.getElementById('opaciteBar2012');

    opaciteBar1990.addEventListener('input', (event) => {
    const opacite = parseFloat(event.target.value);
    couche_1990.setOpacity(opacite);
    });

    opaciteBar2000.addEventListener('input', (event) => {
    const opacite = parseFloat(event.target.value);
    couche_2000.setOpacity(opacite);
    });

    opaciteBar2006.addEventListener('input', (event) => {
    const opacite = parseFloat(event.target.value);
    couche_2006.setOpacity(opacite);
    });

    opaciteBar2012.addEventListener('input', (event) => {
    const opacite = parseFloat(event.target.value);
    couche_2012.setOpacity(opacite);
    });

    
});

const style_couche_1990 = new Style({
    fill: new Fill({
      color: 'rgba(0, 128, 0, 0.3)', // Vert foncé semi-transparent
    }),
    stroke: new Stroke({
      color: 'rgba(0, 128, 0, 0.8)', // Vert foncé
      width: 2,
    }),
});
  
  const style_couche_2000 = new Style({
    fill: new Fill({
      color: 'rgba(255, 255, 0, 0.3)', // Jaune semi-transparent
    }),
    stroke: new Stroke({
      color: 'rgba(255, 255, 0, 0.8)', // Jaune
      width: 2,
    }),
});
  
  const style_couche_2006 = new Style({
    fill: new Fill({
      color: 'rgba(255, 165, 0, 0.3)', // Orange semi-transparent
    }),
    stroke: new Stroke({
      color: 'rgba(255, 165, 0, 0.8)', // Orange
      width: 2,
    }),
});
  
  const style_couche_2012 = new Style({
    fill: new Fill({
      color: 'rgba(255, 0, 0, 0.3)', // Rouge semi-transparent
    }),
    stroke: new Stroke({
      color: 'rgba(255, 0, 0, 0.8)', // Rouge
      width: 2,
    }),
});

