/* This code snippet is a route handler in a Node.js application using Express framework. */
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/', function(req, res, next) {

  console.log(req.body);
  
  // Connexion à la base
  const { Client } = require('pg')
  const connectionString = process.env.DATABASE_URL;

  const client = new Client({
    connectionString: connectionString,
  });

  client.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });



  // Requête

  const id_thematique = req.body.id_thematique;
  console.log('id_thematique : ', id_thematique);
  var requete = `SELECT nom_couche, id_couche FROM public."InfoCouches" WHERE id_thematique = ${id_thematique}`;

  client.query(requete, (err, result) => {
      console.log(result);
      res.json(result.rows);
      client.end();
  });

});

module.exports = router;