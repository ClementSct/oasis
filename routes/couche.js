/* This code snippet defines a POST route that connects to a PostgreSQL database, executes a query
to retrieve data based on the provided `id_couche`, and sends the query result as a JSON response. */
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/', function(req, res,) {
  
  // Connexion à la base
  const { Client } = require('pg')
  const connectionString = process.env.DATABASE_URL;

  const client = new Client({
    connectionString: connectionString,
  });

  client.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });


  //Requête

  console.log('body',req.body);
  
  const { id_couche }  = req.body;
  
  var requete = `SELECT public."InfoCouches".data, public."Styles".fill_color, public."Styles".fill_stroke, public."Styles".stroke_color, public."Styles".style_complexe, public."Styles".attribut
  FROM public."Styles"
  JOIN public."InfoCouches" ON public."Styles".id_simple = public."InfoCouches".id_style_simple
  WHERE public."InfoCouches".id_couche =`+id_couche;


  client.query(requete, (err, result) => {
    //console.log(result.rows);
    res.json(result);
    client.end();
  });
  

});

module.exports = router;
