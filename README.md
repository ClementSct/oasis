<img src="public/images/Logo_Oasis.png" width="256" height="141">

# Démarer l'application

Si vous avez installer l'application,
il faut ouvrir un terminal, se mettre dans le répertoire oasis et lancer "npm run start".
Puis dans un navigateur se rendre à l'URL "localhost:3000"

# Tutoriel d'installation

Tous les contenus de documentation par rapport au projet se trouve dans le dossier "DOCUMENTATION"

## Deux éléments sont nécessaire à l'installation de l'application : l'interface et la base de données

## Installation de l'interface

### Installation de Node.js

Pour installer l'interface, il est nécessaire d'utiliser Node.js
S'il n'est pas encore installé sur votre machine, voici la marche à suivre :

- Se rendre sur le lien suivant : https://nodejs.org/fr
- Allez dans l'onglet "Download"
- Télécharger la version 20, 20.X.Y par exemple
- Cliquer sur le fichier téléchargé et autoriser son exécution
- Suivre la procédure d'installation qui s'affiche en autorisant les conditions générales et en cliquant sur "next"

### Installation des fichiers :

- Cliquer sur l'onglet download en haut à droite de cette page (gitlab, branche main) et sur zip
- Placer ces fichiers sur un répertoire local de votre ordinateur, gardez le bien en tête.

### Lancement de l'interface

- Dans le menu démarrer, chercher "node"
- Cliquer sur l'invite de commande de Node.js : `Node.js commande prompt`
- Pour se placer dans le bon répertoire, taper : `cd chemin_vers_le_répertoire`
  C'est le répertoire où vous avez installé les fichiers  
  Exemple : `cd C:\User\Oasis`
- Taper enter
- Taper ensuite : `npm install`, uniquement lors de la première utilisation
- Taper enter
- Pour lancer le site, taper : `npm start app.js`
- Taper enter
- Il est possible qu'à cette étape la console renvoit une erreur car certains modules ne sont pas installés, comme 'cookie-parser' (le module manquant est affiché dans le message d'erreur). Dans ce cas, taper : `npm install nom_du_mondule_manquant` puis à nouveau : `npm start app.js`.

## Installation de la base de données:

### Installation de postgresql:

https://www.postgresql.org/

Allez dans download et télécharger la version 14, 14.X.Y par exemple.

Toute la base de données est contenue dans un fichier .sql (Il se trouve sur le Gitlab).

En installant PostgreSQL pour la première fois, choisissez "postgres" comme nom d'utilisateur par défaut et aussi pour chaque mot de passe demandé par cette outil.

### Import de la base de données dans postgresql

Si vous avez déjà installer la base de données auparavant, rentrer la commande :

`psql -U nom_utilisateur`

Puis une deuxième commande :

`DROP DATABASE OASIS_26_02_2024`

Dans l'invite de commandes système (pas celui de Node), rentrer la commande suivante :

`psql -U nom_utilisateur -d OASIS -f chemin_vers_le_dossier_oasis/oasis/OASIS_26_02_2024.sql`

nom_utilisateur correspond à votre nom d'utilisateur PostGres (par défaut "postgres").

La base de données est en place, vous pouvez maintenant vous rendre sur un navigateur à l'adresse "localhost:3000" pour profiter de l'application.

# En cas de problème ou de question

Vous pouvez envoyer un e-mail à :
vittorio.toffolutti@gmail.com
