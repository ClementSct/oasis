<img src="public/images/Logo_Oasis.png" width="256" height="141">

# Tutoriel d'utilisation

### Présentation de l'interface

En arrivant sur le démonstrateur OASIS, vous avez face à vous :
- Une colonne à gauche de votre écran où vous pouvez voir toutes les **thématiques Applisat**.
- Une **carte satellite** au milieu de votre écran.
- Une colonne à droite avec le **choix d'affichage de la carte** et une partie de **gestion des couches**.
Vous allez donc pouvoir observer des couches de données sur la carte satellite et pouvoir les gérer dans la gestion des couches.

<img src="DOCUMENTATION/Tutoriels/img/Etape0.png" width="240" height="127">

### Début d'utilisation

Dans un premier temps, sélectionnez une thématique Applisat qui vous intéresse (ex : RISQUE). Un onglet va s'ouvrir avec une liste de couches qui sont liées à la thématique que vous avez choisi. Ensuite, sélectionnez la couche que vous voulez observer/étudier (ex : _Inondations de la Garonne_). Une fois que la couche a été sélectionnée, vous pouvez voir qu'elle apparaît sur la carte satellite et dans la partie Gestion des couches dans la colonne de droite.

<img src="DOCUMENTATION/Tutoriels/img/Etape1.png" width="240" height="127">

### Interactions avec la couche

Vous pouvez donc maintenant utiliser les différentes interactions disponibles pour la couche. Ces interactions se trouvent dans la partie de Gestion des couches, il y en a 5 en tout : 
- La **gestion de la transparence** de la couche avec la barre bleue
- L'**onglet informatif** <img src="public/images/information.png" width="25" height="25"> qui va vous permettre d'avoir différentes informations sur la couche (description générale de la couche, date d'extraction de la couche, description du traitement utilisé pour obtenir la couche, lien vers la fiche Applisat ...).
- La **gestion de la visibilité** de la couche <img src="public/images/oeil_ouvert.png" width="25" height="25">, elle permet de la cacher ou de la faire apparaître.
- Le **recentrage** sur la couche <img src="public/images/center.png" width="25" height="25">, au cas où vous vous éloignez trop de la couche. 
- La **suppression** de la couche <img src="public/images/recycle-bin.png" width="25" height="25">

<img src="DOCUMENTATION/Tutoriels/img/Etape2.png" width="240" height="127">

### Superposition des couches

Vous pouvez aussi **superposer des couches** afin de les comparer/étudier. Étant donné le nombre de couches qui est actuellement limité, vous pouvez seulement superposer les couches _Zones de forêts autour d'un incendie_ (BIODIVERSITÉ) et _Périmètres brûlés pendant et après incendie_ (RISQUE).

<img src="DOCUMENTATION/Tutoriels/img/Etape3.png" width="240" height="127">

### En cas de problème ou de question

N'hésitez pas à regarder la vidéo [Video_Demonstration_OASIS](DOCUMENTATION/Tutoriels/Video_Demonstration_OASIS.mp4)

Vous pouvez aussi envoyer un e-mail à :
oasis_ensg@gmail.com

ou appeler/envoyer un SMS au
07 82 69 13 73

<img src="public/images/ENSG.png" width="80" height="87">       <img src="public/images/Logo_Ministère.png" width="123" height="102">


